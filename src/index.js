import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import App from './components/App';
import './lib/style/index.css';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/(:city)" component={App} />
  </Router>,
  document.getElementById('root')
);
