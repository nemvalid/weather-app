const config = {
  OWM_URL: 'http://api.openweathermap.org/data/2.5/weather',
  OWM_APPID: 'd047853e1f9ee179a4f6076c53843e3b',
  CACHE_EXPIRE: 30
};

export default config;
