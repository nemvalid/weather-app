'use strict';

const mockConfig = {
  OWM_URL: 'http://www.example.com',
  OWM_APPID: '69',
  CACHE_EXPIRE: 1
};

const mockData = {
  weather: {
    foo: 'bar'
  }
};

const mockResponse = {
  weather: {
    foo: 'baz'
  }
};

const mockFetch = () => ({
  then() {
    return {
      then(cb) {
        cb(mockResponse);
      }
    };
  }
});

window.fetch = mockFetch;

import lscache from 'lscache';
import * as config from '../../config';
import OWMSrvc from './index.js';

jest.mock('lscache', () => ({
  get: jest.fn(),
  set: jest.fn()
}));

config.default = mockConfig;

describe('OWMSrvc', () => {

  it('should call lscache with the correct url when looking for a city', () => {
    lscache.get.mockReturnValueOnce(mockData);
    OWMSrvc.getByCity('test');
    expect(lscache.get)
      .toHaveBeenCalledWith('http://www.example.com?units=metric&q=test&APPID=69');
  });

  it('should call lscache with the correct url when looking for a location', () => {
    lscache.get.mockReturnValueOnce(mockData);
    OWMSrvc.getByLocation('LAT', 'LON');
    expect(lscache.get)
      .toHaveBeenCalledWith('http://www.example.com?units=metric&lat=LAT&lon=LON&APPID=69');
  });

  it('should call lscache set', () => {
    lscache.get.mockReturnValueOnce(null);
    OWMSrvc.getByCity('test');
    expect(lscache.set)
      .toHaveBeenCalledWith('http://www.example.com?units=metric&q=test&APPID=69', mockResponse, mockConfig.CACHE_EXPIRE);
  });

});
