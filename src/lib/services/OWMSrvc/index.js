import lscache from 'lscache';
import config from '../../config';

const restHandler = (url) => {
  const localResult = lscache.get(url);
  const result = new Promise((resolve, reject) => {
    if (localResult) {
      resolve(localResult);
      return result;
    }

    fetch(url)
      .then(response => response.json())
      .then(data => {
        if (data.weather) {
          lscache.set(url, data, config.CACHE_EXPIRE);
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch(error => {
        reject(error);
      });
  });

  return result;
};

const OWMSrvc = {
  getByCity(city) {
    const url = `${config.OWM_URL}?units=metric&q=${city}&APPID=${config.OWM_APPID}`;
    return restHandler(url);
  },
  getByLocation(lat, lon) {
    const url = `${config.OWM_URL}?units=metric&lat=${lat}&lon=${lon}&APPID=${config.OWM_APPID}`;
    return restHandler(url);
  }
};

export default OWMSrvc;
