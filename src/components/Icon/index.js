import React from 'react';
import './style/weather-icons.min.css';
import './Icon.css';

const Icon = props => <i className={`weather-icon wi wi-owm-${props.id}`}></i>;

Icon.propTypes = {
  id: React.PropTypes.number.isRequired
};

export default Icon;
