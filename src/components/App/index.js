import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { browserHistory } from 'react-router';
import { geolocated, geoPropTypes } from 'react-geolocated';
import OWMSrvc from '../../lib/services/OWMSrvc';
import './App.css';
import { Widget } from '../Widget';
import Spinner from '../Spinner';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPending: true,
      weather: null
    };
  }

  componentDidMount() {
    if (this.props.params.city) {
      OWMSrvc.getByCity(this.props.params.city)
        .then(result => {
          this.setState({
            weather: result,
            isPending: false
          });
        })
        .catch((reason) => {
          this.setState({
            error: reason,
            weather: null,
            isPending: false
          });
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props === nextProps) {
      return;
    }
    if (!this.props.params.city && nextProps.coords) {
      OWMSrvc.getByLocation(nextProps.coords.latitude, nextProps.coords.longitude)
        .then(result => {
          this.setState({
            weather: result,
            isPending: false
          });
          browserHistory.replace(`/${result.name}`);
        })
        .catch((reason) => {
          this.setState({
            error: reason,
            weather: null,
            isPending: false
          });
        });
    }
  }

  render() {
    return (
      <div className="App">
        <ReactCSSTransitionGroup
          transitionName="animation"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}
          transitionAppear={true}
          transitionAppearTimeout={500}>
          {!this.state.isPending && this.state.weather ?
            <Widget key="widget" weather={this.state.weather} /> :
            <Spinner key="spinner" />}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

App.propTypes = { ...App.propTypes, ...geoPropTypes };

export default geolocated()(App);
