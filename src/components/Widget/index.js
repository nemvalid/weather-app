import React from 'react';
import Icon from '../Icon';
import './Widget.css';

export const tempColor = (temp) => {
  const max = 23;
  const min = 0;
  const t = Math.min(max, Math.max(min, temp)) / max;
  const r = Math.round(240 * t) + 10;
  const g = Math.round((240 * t) / 1.5) + 10;
  const b = 170 - Math.round(170 * t);
  return `rgb(${r}, ${g}, ${b})`;
};

export const Widget = (props) => (
  <div className="weather-widget" style={{ backgroundColor: tempColor(props.weather.main.temp) }}>
    <div className="weather-widget__section weather-widget__section--left">
      <h1 className="weather-widget__city">{props.weather.name}</h1>
      <h3 className="weather-widget__text--details">{props.weather.weather[0].description}</h3>
      <div className="weather-widget__temperature">{props.weather.main.temp.toFixed(1)}°C</div>
    </div>
    <div className="weather-widget__section">
      <Icon id={props.weather.weather[0].id} />
    </div>
    <div className="weather-widget__section">
      <h2 className="weather-widget__text">{props.weather.weather[0].main}</h2>
      <div className="weather-widget__pressure text-left"><i className="wi wi-barometer"></i>
        {props.weather.main.pressure} hPa
      </div>
      <div className="weather-widget__humidity text-left"><i className="wi wi-humidity"></i>
        {props.weather.main.humidity}%
      </div>
      <div className="weather-widget__wind text-left"><i className="wi wi-small-craft-advisory"></i>
        {props.weather.wind.deg}°  {props.weather.wind.speed} km/h
      </div>


    </div>
  </div>
);

Widget.propTypes = {
  weather: React.PropTypes.object.isRequired
};
