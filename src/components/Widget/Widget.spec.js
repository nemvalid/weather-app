'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import { tempColor, Widget } from './index';

describe('tempColor', () => {
  it('should convert temperature to rgb', () => {
    expect(tempColor(0)).toBe('rgb(10, 10, 170)');
    expect(tempColor(-20)).toBe('rgb(10, 10, 170)');
    expect(tempColor(15)).toBe('rgb(167, 114, 59)');
    expect(tempColor(23)).toBe('rgb(250, 170, 0)');
    expect(tempColor(50)).toBe('rgb(250, 170, 0)');
  });
});

describe('Widget', () => {
  const mockWeather = {
    main: {
      temp: 10
    },
    name: 'TEST',
    weather: {
      0: {
        id: 802
      }
    }
  };

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Widget weather={mockWeather}/>, div);
  });
});
