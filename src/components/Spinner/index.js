import React from 'react';
import './Spinner.css';

const Spinner = () => (<div className="weather-loader"></div>);

export default Spinner;
